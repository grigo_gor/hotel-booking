package com.example.grigo.hotelbookingsystem.db.objects;

import java.io.Serializable;

/**
 * Created by grigo on 12/17/2017.
 */

public class Hotel implements Serializable{
    private int id;
    private String name;
    private String price;
    private String city;
    private float rating;

    public Hotel(int id, String name, String price, String city, float rating) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.city = city;
        this.rating = rating;
    }

    public Hotel(String name, String price, String city, float rating) {
        this.name = name;
        this.price = price;
        this.city = city;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getRaiting() {
        return rating;
    }

    public void setRaiting(float raiting) {
        this.rating = raiting;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", city='" + city + '\'' +
                ", raiting=" + rating +
                '}';
    }
}
