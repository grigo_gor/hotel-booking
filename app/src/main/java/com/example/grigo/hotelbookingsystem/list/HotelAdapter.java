package com.example.grigo.hotelbookingsystem.list;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grigo.hotelbookingsystem.R;
import com.example.grigo.hotelbookingsystem.activities.NewScheduleHotelActivity;
import com.example.grigo.hotelbookingsystem.db.objects.Hotel;
import com.example.grigo.hotelbookingsystem.utils.UserInfo;

import java.util.List;

/**
 * Created by grigo on 12/20/2017.
 */

public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.MyViewHolder> {

    private List<Hotel> hotelsList;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, price, city, rating;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.hotel_name_text);
            price = (TextView) view.findViewById(R.id.hotel_price_text);
            city = (TextView) view.findViewById(R.id.hotel_city_text);
            rating = (TextView) view.findViewById(R.id.hotel_rating_text);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(!UserInfo.IS_ADMIN) {
                Intent i = new Intent(v.getContext(), NewScheduleHotelActivity.class);
                i.putExtra("hotel", hotelsList.get(getPosition()));
                v.getContext().startActivity(i);
            }
        }
    }


    public HotelAdapter(List<Hotel> hotelsList) {
        this.hotelsList = hotelsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hotel_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Hotel hotel = hotelsList.get(position);
        holder.name.setText(hotel.getName());
        holder.price.setText(hotel.getPrice());
        holder.city.setText(hotel.getCity());
        holder.rating.setText(hotel.getRaiting()+"");
    }

    @Override
    public int getItemCount() {
        return hotelsList.size();
    }
}