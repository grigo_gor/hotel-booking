package com.example.grigo.hotelbookingsystem.db;

import android.content.Context;

import com.example.grigo.hotelbookingsystem.db.executors.HotelQueries;
import com.example.grigo.hotelbookingsystem.db.executors.ScheduledHotelQueries;
import com.example.grigo.hotelbookingsystem.db.executors.UserQueries;
import com.example.grigo.hotelbookingsystem.db.executors.functionality.IHotelQueries;
import com.example.grigo.hotelbookingsystem.db.executors.functionality.IScheduledHotelQueries;
import com.example.grigo.hotelbookingsystem.db.executors.functionality.IUserQueries;
import com.example.grigo.hotelbookingsystem.db.objects.Hotel;
import com.example.grigo.hotelbookingsystem.db.objects.ScheduledHotel;
import com.example.grigo.hotelbookingsystem.db.objects.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grigo on 12/17/2017.
 */

public class HotelDatabase implements IScheduledHotelQueries, IHotelQueries, IUserQueries {

    private static HotelQueries hq;
    private static ScheduledHotelQueries shq;
    private static UserQueries uq;

    private static HotelDatabase db;

    private HotelDatabase(Context context){
        hq = new HotelQueries(context);
        shq = new ScheduledHotelQueries(context);
        uq = new UserQueries(context);
    }

    public static HotelDatabase getInstance(Context context){
        if(db == null){
            db = new HotelDatabase(context);
        }
        return db;
    }

    @Override
    public int addHotel(Hotel h) {
        hq.open();
        int id = hq.addHotel(h);
        hq.close();
        return id;
    }

    @Override
    public void editHotel(Hotel h) {
        hq.open();
        hq.editHotel(h);
        hq.close();
    }

    @Override
    public void deleteHotel(int id) {
        hq.open();
        hq.deleteHotel(id);
        hq.close();
    }

    @Override
    public Hotel getHotelByID(int id) {
        hq.open();
        Hotel h = hq.getHotelByID(id);
        hq.close();
        return h;
    }

    @Override
    public List<Hotel> getAllHotels() {
        hq.open();
        List<Hotel> hotels = hq.getAllHotels();
        hq.close();
        return hotels;
    }

    @Override
    public int addScheduledHotel(ScheduledHotel h) {
        shq.open();
        int id = shq.addScheduledHotel(h);
        shq.close();
        return id;
    }

    @Override
    public void editScheduledHotel(ScheduledHotel h) {
        shq.open();
        shq.editScheduledHotel(h);
        shq.close();
    }

    @Override
    public void deleteScheduledHotel(int id) {
        shq.open();
        shq.deleteScheduledHotel(id);
        shq.close();
    }

    @Override
    public ScheduledHotel getScheduledHotelByID(int id) {
        shq.open();
        ScheduledHotel h = shq.getScheduledHotelByID(id);
        shq.close();
        return h;
    }

    @Override
    public List<ScheduledHotel> getAllScheduledHotelsOfUser(int id) {
        shq.open();
        List<ScheduledHotel> hotels = shq.getAllScheduledHotelsOfUser(id);
        shq.close();
        return hotels;
    }

    @Override
    public int addUser(User u) {
        uq.open();
        int id = uq.addUser(u);
        uq.close();
        return id;
    }

    @Override
    public void deleteUser(int id) {
        uq.open();
        uq.deleteUser(id);
        uq.close();
    }

    @Override
    public User getUserByID(int id) {
        uq.open();
        User u = uq.getUserByID(id);
        uq.close();
        return u;
    }

    @Override
    public List<User> getAllUsers() {
        uq.open();
        List<User> users = uq.getAllUsers();
        uq.close();
        return users;
    }

    @Override
    public boolean checkIfUserExists(String username) {
        uq.open();
        boolean b = uq.checkIfUserExists(username);
        uq.close();
        return b;
    }
}
