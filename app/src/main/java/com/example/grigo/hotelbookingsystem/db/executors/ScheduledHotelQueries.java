package com.example.grigo.hotelbookingsystem.db.executors;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.grigo.hotelbookingsystem.db.DBHelper;
import com.example.grigo.hotelbookingsystem.db.executors.functionality.IScheduledHotelQueries;
import com.example.grigo.hotelbookingsystem.db.objects.ScheduledHotel;
import com.example.grigo.hotelbookingsystem.db.tables.ScheduledHotelsTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grigo on 12/17/2017.
 */

public class ScheduledHotelQueries implements IScheduledHotelQueries {
    private String[] allColumns = {
            ScheduledHotelsTable.COLUMN_ID,
            ScheduledHotelsTable.COLUMN_HOTEL_ID,
            ScheduledHotelsTable.COLUMN_START,
            ScheduledHotelsTable.COLUMN_END,
            ScheduledHotelsTable.COLUMN_USERID
    };

    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public ScheduledHotelQueries(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    @Override
    public int addScheduledHotel(ScheduledHotel h) {
        ContentValues values = new ContentValues();
        values.put(ScheduledHotelsTable.COLUMN_HOTEL_ID, h.getHotelID());
        values.put(ScheduledHotelsTable.COLUMN_START, h.getStartDate());
        values.put(ScheduledHotelsTable.COLUMN_END, h.getEndDate());
        values.put(ScheduledHotelsTable.COLUMN_USERID, h.getUserID());
        int id = (int) database.insert(ScheduledHotelsTable.TABLE_NAME, null, values);
        Cursor cursor = database.query(ScheduledHotelsTable.TABLE_NAME, allColumns, ScheduledHotelsTable.COLUMN_ID + " = " + id, null, null, null, null);
        cursor.moveToFirst();
        int res = cursor.getInt(0);
        cursor.close();
        return res;
    }

    @Override
    public void editScheduledHotel(ScheduledHotel h) {
        ContentValues values = new ContentValues();
        values.put(ScheduledHotelsTable.COLUMN_HOTEL_ID, h.getHotelID());
        values.put(ScheduledHotelsTable.COLUMN_START, h.getStartDate());
        values.put(ScheduledHotelsTable.COLUMN_END, h.getEndDate());
        values.put(ScheduledHotelsTable.COLUMN_USERID, h.getUserID());
        database.update(ScheduledHotelsTable.TABLE_NAME, values, ScheduledHotelsTable.COLUMN_ID + " = " + h.getId(), null);
    }

    @Override
    public void deleteScheduledHotel(int id) {
        database.delete(ScheduledHotelsTable.TABLE_NAME, ScheduledHotelsTable.COLUMN_ID + " = " + id, null);

    }

    @Override
    public ScheduledHotel getScheduledHotelByID(int id) {
        ScheduledHotel h = null;
        Cursor cursor = database.query(ScheduledHotelsTable.TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (cursor.getInt(0) == id) {
                h = new ScheduledHotel(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4));
            }
            cursor.moveToNext();
        }
        cursor.close();
        return h;
    }

    @Override
    public List<ScheduledHotel> getAllScheduledHotelsOfUser(int id) {
        List<ScheduledHotel> scheduledHotels = new ArrayList<>();
        Cursor cursor = database.query(ScheduledHotelsTable.TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (cursor.getInt(4) == id) {
                scheduledHotels.add(new ScheduledHotel(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4)));
            }
            cursor.moveToNext();
        }
        cursor.close();
        return scheduledHotels;
    }
}
