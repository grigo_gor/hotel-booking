package com.example.grigo.hotelbookingsystem.db.executors;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.grigo.hotelbookingsystem.db.DBHelper;
import com.example.grigo.hotelbookingsystem.db.executors.functionality.IUserQueries;
import com.example.grigo.hotelbookingsystem.db.objects.User;
import com.example.grigo.hotelbookingsystem.db.tables.UsersTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grigo on 12/19/2017.
 */

public class UserQueries implements IUserQueries {

    private String[] allColumns = {
            UsersTable.COLUMN_ID,
            UsersTable.COLUMN_NAME,
            UsersTable.COLUMN_USERNAME,
            UsersTable.COLUMN_PASSWORD
    };

    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public UserQueries(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    @Override
    public int addUser(User u) {
        ContentValues values = new ContentValues();
        values.put(UsersTable.COLUMN_NAME, u.getName());
        values.put(UsersTable.COLUMN_USERNAME, u.getUserName());
        values.put(UsersTable.COLUMN_PASSWORD, u.getPassword());
        int id = (int) database.insert(UsersTable.TABLE_NAME, null, values);
        Cursor cursor = database.query(UsersTable.TABLE_NAME, allColumns, UsersTable.COLUMN_ID + " = " + id, null, null, null, null);
        cursor.moveToFirst();
        int res = cursor.getInt(0);
        cursor.close();
        return res;
    }

    @Override
    public void deleteUser(int id) {
        database.delete(UsersTable.TABLE_NAME, UsersTable.COLUMN_ID + " = " + id, null);
    }

    @Override
    public User getUserByID(int id) {
        User u = null;
        Cursor cursor = database.query(UsersTable.TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (cursor.getInt(0) == id) {
                u = new User(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
            }
            cursor.moveToNext();
        }
        cursor.close();
        return u;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        Cursor cursor = database.query(UsersTable.TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            users.add(new User(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3)));
            cursor.moveToNext();
        }
        cursor.close();
        return users;
    }

    //TODO FIX NEEDED
    @Override
    public boolean checkIfUserExists(String username) {
        Cursor cursor = null;
        try {
            cursor = database.rawQuery("SELECT * FROM " + UsersTable.TABLE_NAME + " where " + UsersTable.COLUMN_USERNAME + " = " + username, null);
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            cursor.close();
        }
        return false;
    }
}
