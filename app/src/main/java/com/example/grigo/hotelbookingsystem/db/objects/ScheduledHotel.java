package com.example.grigo.hotelbookingsystem.db.objects;

/**
 * Created by grigo on 12/17/2017.
 */

public class ScheduledHotel{
    private int id;
    private int hotelID;
    private String startDate;
    private String endDate;
    private int userID;

    public ScheduledHotel(int id, int hotelID, String startDate, String endDate, int userID) {
        this.id = id;
        this.hotelID = hotelID;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userID = userID;
    }

    public ScheduledHotel(int hotelID, String startDate, String endDate, int userID) {
        this.hotelID = hotelID;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userID = userID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHotelID() {
        return hotelID;
    }

    public void setHotelID(int hotelID) {
        this.hotelID = hotelID;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return "ScheduledHotel{" +
                "id=" + id +
                ", hotelID=" + hotelID +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
