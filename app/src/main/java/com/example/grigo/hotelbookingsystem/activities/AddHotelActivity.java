package com.example.grigo.hotelbookingsystem.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.grigo.hotelbookingsystem.R;
import com.example.grigo.hotelbookingsystem.db.HotelDatabase;
import com.example.grigo.hotelbookingsystem.db.objects.Hotel;

public class AddHotelActivity extends AppCompatActivity {

    private EditText name, price, city, rating;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hotel);
        name = (EditText) findViewById(R.id.new_hotel_name);
        price = (EditText) findViewById(R.id.new_hotel_price);
        city = (EditText) findViewById(R.id.new_hotel_city);
        rating = (EditText) findViewById(R.id.new_hotel_rating);
        saveButton = (Button) findViewById(R.id.save_new_hotel);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    Hotel h = new Hotel(name.getText().toString(), price.getText().toString(), city.getText().toString(), Float.parseFloat(rating.getText().toString()));
                    HotelDatabase.getInstance(getApplicationContext()).addHotel(h);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Wrong parameters", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }




    private boolean validate(){
        boolean valid = true;
        if(name.getText().toString().equals("") || price.getText().toString().equals("") || city.getText().toString().equals("") || rating.getText().toString().equals("")){
            valid = false;
        }
        return valid;
    }
}
