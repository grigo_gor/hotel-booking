package com.example.grigo.hotelbookingsystem.list;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.grigo.hotelbookingsystem.R;
import com.example.grigo.hotelbookingsystem.activities.NewScheduleHotelActivity;
import com.example.grigo.hotelbookingsystem.db.HotelDatabase;
import com.example.grigo.hotelbookingsystem.db.objects.Hotel;
import com.example.grigo.hotelbookingsystem.db.objects.ScheduledHotel;
import com.example.grigo.hotelbookingsystem.utils.UserInfo;

import java.util.List;

/**
 * Created by grigo on 2/14/2018.
 */

public class ScheduledHotelsAdapter extends RecyclerView.Adapter<ScheduledHotelsAdapter.MyViewHolder> {

    private List<ScheduledHotel> hotelsList;
    private HotelDatabase db;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView name, price, city, rating, start, end;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.hotel_name_text);
            price = (TextView) view.findViewById(R.id.hotel_price_text);
            city = (TextView) view.findViewById(R.id.hotel_city_text);
            rating = (TextView) view.findViewById(R.id.hotel_rating_text);
            start = (TextView) view.findViewById(R.id.hotel_start_text);
            end = (TextView) view.findViewById(R.id.hotel_end_text);
            db = HotelDatabase.getInstance(view.getContext());
        }
    }


    public ScheduledHotelsAdapter(List<ScheduledHotel> hotelsList) {
        this.hotelsList = hotelsList;
    }

    @Override
    public ScheduledHotelsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.scheduled_hotel_list_item, parent, false);

        return new ScheduledHotelsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ScheduledHotelsAdapter.MyViewHolder holder, int position) {
        ScheduledHotel hotel = hotelsList.get(position);
        Hotel h = db.getHotelByID(hotel.getId());
        holder.name.setText(h.getName());
        holder.price.setText(h.getPrice());
        holder.city.setText(h.getCity());
        holder.rating.setText(h.getRaiting()+"");
        holder.start.setText(hotel.getStartDate());
        holder.end.setText(hotel.getEndDate());
    }

    @Override
    public int getItemCount() {
        return hotelsList.size();
    }
}