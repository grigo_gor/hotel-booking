package com.example.grigo.hotelbookingsystem.db.tables;

/**
 * Created by grigo on 12/17/2017.
 */

public class HotelsTable {
    public static final String TABLE_NAME = "hbs_hotels_table";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_RATING = "rating";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME +
            "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_PRICE + " text not null, "
            + COLUMN_CITY + " text not null, "
            + COLUMN_RATING + " real not null" + ");";
}
