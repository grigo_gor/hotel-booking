package com.example.grigo.hotelbookingsystem.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.grigo.hotelbookingsystem.R;
import com.example.grigo.hotelbookingsystem.db.HotelDatabase;
import com.example.grigo.hotelbookingsystem.db.objects.Hotel;
import com.example.grigo.hotelbookingsystem.db.objects.ScheduledHotel;
import com.example.grigo.hotelbookingsystem.list.HotelAdapter;
import com.example.grigo.hotelbookingsystem.list.ScheduledHotelsAdapter;
import com.example.grigo.hotelbookingsystem.utils.UserInfo;

import java.util.ArrayList;
import java.util.List;

public class ScheduledOrdersActivity extends AppCompatActivity {

    private List<ScheduledHotel> hotelsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ScheduledHotelsAdapter mAdapter;
    private HotelDatabase db = HotelDatabase.getInstance(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheduled_orders);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_sch);
        hotelsList = db.getAllScheduledHotelsOfUser(UserInfo.IS_USER1 ? 2 : 3);
        mAdapter = new ScheduledHotelsAdapter(hotelsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter.notifyDataSetChanged();
    }
}
