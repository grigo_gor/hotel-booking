package com.example.grigo.hotelbookingsystem.db.executors.functionality;

import com.example.grigo.hotelbookingsystem.db.objects.User;

import java.util.List;

/**
 * Created by grigo on 12/19/2017.
 */

public interface IUserQueries {
    int addUser(User u);
    //void editUser(User u);
    void deleteUser(int id);
    User getUserByID(int id);
    List<User> getAllUsers();
    boolean checkIfUserExists(String username);
}
