package com.example.grigo.hotelbookingsystem.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.grigo.hotelbookingsystem.db.tables.HotelsTable;
import com.example.grigo.hotelbookingsystem.db.tables.ScheduledHotelsTable;
import com.example.grigo.hotelbookingsystem.db.tables.UsersTable;

/**
 * Created by grigo on 12/17/2017.
 */

public class DBHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME = "hotel_booking_system_rau.db";
    public static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(HotelsTable.CREATE_TABLE);
        db.execSQL(ScheduledHotelsTable.CREATE_TABLE);
        db.execSQL(UsersTable.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + HotelsTable.TABLE_NAME);
        db.execSQL("drop table if exists " + ScheduledHotelsTable.TABLE_NAME);
        db.execSQL("drop table if exists " + UsersTable.TABLE_NAME);
    }
}
