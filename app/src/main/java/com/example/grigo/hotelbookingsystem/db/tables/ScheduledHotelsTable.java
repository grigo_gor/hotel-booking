package com.example.grigo.hotelbookingsystem.db.tables;

/**
 * Created by grigo on 12/17/2017.
 */

public class ScheduledHotelsTable {
    public static final String TABLE_NAME = "hbs_scheduled_hotels_table";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_HOTEL_ID = "hotel_id";
    public static final String COLUMN_START = "start";
    public static final String COLUMN_END = "end";
    public static final String COLUMN_USERID = "user_id";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME +
            "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_HOTEL_ID + " integer not null, "
            + COLUMN_START + " text not null, "
            + COLUMN_END + " text not null, "
            + COLUMN_USERID + " integer" + ");";
}
