package com.example.grigo.hotelbookingsystem.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grigo.hotelbookingsystem.R;
import com.example.grigo.hotelbookingsystem.db.HotelDatabase;
import com.example.grigo.hotelbookingsystem.db.objects.Hotel;
import com.example.grigo.hotelbookingsystem.db.objects.ScheduledHotel;
import com.example.grigo.hotelbookingsystem.utils.UserInfo;

import org.w3c.dom.Text;

public class NewScheduleHotelActivity extends AppCompatActivity {

    private ScheduledHotel schHotel;
    private Hotel hotel;

    private Button submit, cancel;
    private TextView name, price, rating, location;
    private EditText start, end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_schedule_hotel);
        Intent i = getIntent();
        hotel = (Hotel) i.getExtras().get("hotel");


        name = (TextView) findViewById(R.id.hotel_sch_name);
        price = (TextView) findViewById(R.id.hotel_sch_price);
        rating = (TextView) findViewById(R.id.hotel_sch_rating);
        location = (TextView) findViewById(R.id.hotel_sch_location);

        submit = (Button) findViewById(R.id.hotel_sch_submit);
        cancel = (Button) findViewById(R.id.hotel_sch_cancel);

        start = (EditText) findViewById(R.id.hotel_sch_start);
        end = (EditText) findViewById(R.id.hotel_sch_end);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    int userID = 0;

                    userID = UserInfo.IS_USER1 ? 2 : 3;

                    schHotel = new ScheduledHotel(hotel.getId(), start.getText().toString(), end.getText().toString(), userID);
                    Log.e("ID", hotel.getId() + "");
                    HotelDatabase.getInstance(getApplicationContext()).addScheduledHotel(schHotel);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Wrong parameters", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        name.setText(hotel.getName());
        price.setText(hotel.getPrice());
        rating.setText(hotel.getRaiting()+"");
        location.setText(hotel.getCity());
    }

    private boolean validate(){
        boolean valid = true;
        if(start.getText().toString().equals("") || end.getText().toString().equals("")){
            valid = false;
        }
        return valid;
    }
}
