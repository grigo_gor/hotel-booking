package com.example.grigo.hotelbookingsystem.db.executors;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.grigo.hotelbookingsystem.db.DBHelper;
import com.example.grigo.hotelbookingsystem.db.executors.functionality.IHotelQueries;
import com.example.grigo.hotelbookingsystem.db.objects.Hotel;
import com.example.grigo.hotelbookingsystem.db.tables.HotelsTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grigo on 12/17/2017.
 */

public class HotelQueries implements IHotelQueries {

    private String[] allColumns = {
            HotelsTable.COLUMN_ID,
            HotelsTable.COLUMN_NAME,
            HotelsTable.COLUMN_PRICE,
            HotelsTable.COLUMN_CITY,
            HotelsTable.COLUMN_RATING
    };

    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public HotelQueries(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    @Override
    public int addHotel(Hotel h) {
        ContentValues values = new ContentValues();
        values.put(HotelsTable.COLUMN_NAME, h.getName());
        values.put(HotelsTable.COLUMN_PRICE, h.getPrice());
        values.put(HotelsTable.COLUMN_CITY, h.getCity());
        values.put(HotelsTable.COLUMN_RATING, h.getRaiting());
        int id = (int) database.insert(HotelsTable.TABLE_NAME, null, values);
        Cursor cursor = database.query(HotelsTable.TABLE_NAME, allColumns, HotelsTable.COLUMN_ID + " = " + id, null, null, null, null);
        cursor.moveToFirst();
        int res = cursor.getInt(0);
        cursor.close();
        return res;
    }

    @Override
    public void editHotel(Hotel h) {
        ContentValues values = new ContentValues();
        values.put(HotelsTable.COLUMN_NAME, h.getName());
        values.put(HotelsTable.COLUMN_PRICE, h.getPrice());
        values.put(HotelsTable.COLUMN_CITY, h.getCity());
        values.put(HotelsTable.COLUMN_RATING, h.getRaiting());
        database.update(HotelsTable.TABLE_NAME, values, HotelsTable.COLUMN_ID + " = " + h.getId(), null);
    }

    @Override
    public void deleteHotel(int id) {
        database.delete(HotelsTable.TABLE_NAME, HotelsTable.COLUMN_ID + " = " + id, null);
    }

    @Override
    public Hotel getHotelByID(int id) {
        Hotel h = null;
        Cursor cursor = database.query(HotelsTable.TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (cursor.getInt(0) == id) {
                h = new Hotel(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getFloat(4));
            }
            cursor.moveToNext();
        }
        cursor.close();
        return h;
    }

    @Override
    public List<Hotel> getAllHotels() {
        List<Hotel> hotels = new ArrayList<Hotel>();
        Cursor cursor = database.query(HotelsTable.TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            hotels.add(new Hotel(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getFloat(4)));
            cursor.moveToNext();
        }
        cursor.close();
        return hotels;
    }
}
