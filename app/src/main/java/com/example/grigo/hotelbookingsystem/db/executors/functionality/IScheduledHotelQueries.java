package com.example.grigo.hotelbookingsystem.db.executors.functionality;

import com.example.grigo.hotelbookingsystem.db.objects.ScheduledHotel;

import java.util.List;

/**
 * Created by grigo on 12/17/2017.
 */

public interface IScheduledHotelQueries {
    int addScheduledHotel(ScheduledHotel h);
    void editScheduledHotel(ScheduledHotel h);
    void deleteScheduledHotel(int id);
    ScheduledHotel getScheduledHotelByID(int id);
    List<ScheduledHotel> getAllScheduledHotelsOfUser(int id);
}
