package com.example.grigo.hotelbookingsystem.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grigo.hotelbookingsystem.R;
import com.example.grigo.hotelbookingsystem.db.HotelDatabase;
import com.example.grigo.hotelbookingsystem.db.objects.User;
import com.example.grigo.hotelbookingsystem.utils.UserInfo;

public class LoginActivity extends AppCompatActivity {

    private EditText _usernameText;
    private EditText _passwordText;
    private Button _loginButton;

    private String username;
    private String password;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        forFirstRun();
        _usernameText = (EditText) findViewById(R.id.input_username);
        _passwordText = (EditText) findViewById(R.id.input_password);
        _loginButton = (Button) findViewById(R.id.btn_login);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    public void login() {
        username = _usernameText.getText().toString();
        password = _passwordText.getText().toString();
        if (!validate()) {
            onLoginFailed();
            return;
        }
        onLoginSuccess();
        /*
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        onLoginSuccess();
                        progressDialog.dismiss();
                    }
                }, 3000);*/
    }

    public void onLoginSuccess() {
        UserInfo.IS_ADMIN = username.equals("admin") && password.equals("admin");
        UserInfo.IS_USER1 = (username.equals("user1") && password.equals("user1"));
        // if successfully logged in and user1 is false, then only user2 can be true
        if(UserInfo.IS_ADMIN || UserInfo.IS_USER1 || (username.equals("user2") && password.equals("user2"))) {
            Intent i = new Intent(this, HotelsListActivity.class);
            startActivity(i);
        }
        else {
            onLoginFailed();
        }
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_SHORT).show();
    }

    public boolean validate() {
        boolean valid = true;

        if (password.isEmpty()) {
            _passwordText.setError("password is empty");
            valid = false;
        }

        if (username.isEmpty()) {
            _usernameText.setError("username is empty");
            valid = false;
        }

        return valid;
    }
    private void forFirstRun(){
        if(HotelDatabase.getInstance(this).getAllUsers() == null || HotelDatabase.getInstance(this).getAllUsers().size() == 0){
            HotelDatabase.getInstance(this).addUser(new User("Gor", "admin", "admin"));
            HotelDatabase.getInstance(this).addUser(new User("Arman", "user1", "user1"));
            HotelDatabase.getInstance(this).addUser(new User("Elen", "user2", "user2"));
        }
    }
}
