package com.example.grigo.hotelbookingsystem.db.tables;

/**
 * Created by grigo on 12/19/2017.
 */

public class UsersTable {
    public static final String TABLE_NAME = "hbs_users_table";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_PASSWORD = "password";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME +
            "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_USERNAME + " text not null, "
            + COLUMN_PASSWORD + " text not null" + ");";
}
