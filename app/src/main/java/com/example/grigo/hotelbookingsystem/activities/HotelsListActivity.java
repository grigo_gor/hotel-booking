package com.example.grigo.hotelbookingsystem.activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.example.grigo.hotelbookingsystem.R;
import com.example.grigo.hotelbookingsystem.db.HotelDatabase;
import com.example.grigo.hotelbookingsystem.db.objects.Hotel;
import com.example.grigo.hotelbookingsystem.db.objects.ScheduledHotel;
import com.example.grigo.hotelbookingsystem.list.HotelAdapter;
import com.example.grigo.hotelbookingsystem.utils.UserInfo;

import java.util.ArrayList;
import java.util.List;

public class HotelsListActivity extends AppCompatActivity {

    private List<Hotel> hotelsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private HotelAdapter mAdapter;
    private HotelDatabase db = HotelDatabase.getInstance(this);
    private FloatingActionButton addButton;
    private Button scheduledHotelsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotels_list);
        forFirstRun();
        addButton = (FloatingActionButton) findViewById(R.id.add_hotel_button);
        scheduledHotelsButton = (Button) findViewById(R.id.scheduled_hotels_button);
        addButton.setVisibility(UserInfo.IS_ADMIN ? View.VISIBLE : View.GONE);
        scheduledHotelsButton.setVisibility(UserInfo.IS_ADMIN ? View.GONE : View.VISIBLE);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        hotelsList = db.getAllHotels();
        mAdapter = new HotelAdapter(hotelsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter.notifyDataSetChanged();

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddHotelActivity.class);
                startActivity(i);
            }
        });

        scheduledHotelsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ScheduledOrdersActivity.class);
                startActivity(i);
            }
        });
    }

    private void forFirstRun() {

        if (db.getAllHotels() == null || db.getAllHotels().size() == 0) {
            db.addHotel(new Hotel("Hotel1", "22.000", "Yerevan", 4.2f));
            db.addHotel(new Hotel("Hotel2", "23.000", "Yerevan", 4.3f));
            db.addHotel(new Hotel("Hotel3", "24.000", "Yerevan", 4.4f));
            db.addHotel(new Hotel("Hotel4", "25.000", "Yerevan", 4.5f));
            db.addHotel(new Hotel("Hotel5", "26.000", "Yerevan", 4.6f));
            db.addHotel(new Hotel("Hotel6", "27.000", "Yerevan", 4.7f));
            db.addHotel(new Hotel("Hotel7", "28.000", "Yerevan", 4.8f));
            db.addHotel(new Hotel("Hotel8", "29.000", "Yerevan", 4.9f));
            db.addHotel(new Hotel("Hotel9", "21.000", "Yerevan", 4.1f));
            db.addHotel(new Hotel("Hotel10", "10.000", "Yerevan", 4.4f));
            db.addHotel(new Hotel("Hotel11", "10.000", "Yerevan", 4.2f));
            db.addHotel(new Hotel("Hotel12", "10.000", "Yerevan", 4.4f));
            db.addHotel(new Hotel("Hotel13", "50.000", "Yerevan", 4.3f));
        }
    }
}
