package com.example.grigo.hotelbookingsystem.db.executors.functionality;

import com.example.grigo.hotelbookingsystem.db.objects.Hotel;

import java.util.List;

/**
 * Created by grigo on 12/17/2017.
 */

public interface IHotelQueries {
    int addHotel(Hotel h);
    void editHotel(Hotel h);
    void deleteHotel(int id);
    Hotel getHotelByID(int id);
    List<Hotel> getAllHotels();
}
